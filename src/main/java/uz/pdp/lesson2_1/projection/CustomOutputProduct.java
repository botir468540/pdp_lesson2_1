package uz.pdp.lesson2_1.projection;


import org.springframework.data.rest.core.config.Projection;
import uz.pdp.lesson2_1.entity.*;

@Projection(types = OutputProduct.class)
public interface CustomOutputProduct {
    Integer getId();

    Product getProduct();

    Double getAmount();

    Double getPrice();

    Output getOutput();
}
