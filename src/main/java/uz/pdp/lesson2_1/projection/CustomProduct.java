package uz.pdp.lesson2_1.projection;


import org.springframework.data.rest.core.config.Projection;
import uz.pdp.lesson2_1.entity.Attachment;
import uz.pdp.lesson2_1.entity.Product;

@Projection(types = Product.class)
public interface CustomProduct {
    Integer getId();

    String getName();

    boolean getActive();

    CustomCategory getCategory();

    Attachment getPhoto();

    String getCode();

    CustomMeasurement getMeasurement();
}
